import React from "react";
import "./UserItem.css";
import UserItem from "../components/UserItem";
import Card from "../../shared/components/UIElements/Card";

const UsersList = (props) => {
  if (!props.items.length) {
    return (
      <Card>
        <div className="center">No User Found!</div>;
      </Card>
    );
  }
  return (
    <ul className="users-list">
      {props.items.map((user) => (
        <UserItem
          key={user.id}
          id={user.id}
          image={user.image}
          name={user.name}
          placeCount={user.places}
        />
      ))}
    </ul>
  );
};
export default UsersList;

import React, { useContext, useState } from "react";
import Input from "../../shared/components/FormElements/Input";
import {
  VALIDATOR_MINLENGTH,
  VALIDATOR_EMAIL,
  VALIDATOR_REQUIRE,
} from "../../shared/util/validators";
import { useForm } from "../../shared/custom-hooks/form-hook";
import Button from "../../shared/components/FormElements/Button";
import Card from "../../shared/components/UIElements/Card";
import { AuthContext } from "../../shared/context/auth-context";
import "./Authentication.css";

const Authentication = (props) => {
  const auth = useContext(AuthContext);

  const [isLoginMode, setIsLoginMode] = useState(true);
  const [formState, inputHandler, setFormData] = useForm(
    {
      email: { value: "", isValid: false },
      password: { value: "", isValid: false },
    },
    false
  );
  const submitHandler = (e) => {
    e.preventDefault();
    console.log(formState.inputs);
    auth.logIn();
  };

  const switchLoginModeHandler = () => {
    if (!isLoginMode) {
      //switching to login mode
      setFormData(
        {
          ...formState.inputs,
          name: undefined,
        },
        formState.inputs.email.isValid && formState.inputs.password.isValid
      );
    } else {
      setFormData(
        {
          ...formState.inputs,
          name: {
            value: "",
            isValid: false,
          },
        },
        false
      );
    }
    setIsLoginMode((prevVal) => !prevVal);
  };
  return (
    <Card className="authentication" style={{ padding: "1rem" }}>
      <h2>Login required</h2>
      <hr />
      <form onSubmit={submitHandler}>
        {!isLoginMode && (
          <Input
            type="name"
            element="input"
            id="name"
            label="Full Name"
            placeholder="Full Name"
            onInput={inputHandler}
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a name"
          />
        )}
        <Input
          type="email"
          element="input"
          id="email"
          placeholder="E-mail"
          label="Email"
          onInput={inputHandler}
          validators={[VALIDATOR_REQUIRE(), VALIDATOR_EMAIL()]}
          errorText="Please enter a valid email"
          autoComplete={false}
          value={formState.inputs.email.value}
        />
        <Input
          type="password"
          element="input"
          id="password"
          placeholder="Password"
          label="Password"
          onInput={inputHandler}
          validators={[VALIDATOR_REQUIRE(), VALIDATOR_MINLENGTH(6)]}
          errorText="Please use password with minimum 6 characters "
          value={formState.inputs.password.value}
        />
        <Button type="submit" disabled={!formState.isValid}>
          {isLoginMode ? "Login" : "Sign-up"}
        </Button>
      </form>
      <Button inverse onClick={switchLoginModeHandler}>
        {" "}
        Switch to {isLoginMode ? "Sign up" : "Login"}
      </Button>
    </Card>
  );
};

export default Authentication;

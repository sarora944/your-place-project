import React from "react";
import UsersList from "../components/UsersList";

const Users = () => {
  const USERS = [
    {
      id: "u1",
      name: "Batman",
      image: "https://i.scdn.co/image/ab67706c0000bebb4f53bb8d9c0354410783e9f6",
      places: 50,
    },
  ];
  return <UsersList items={USERS} />;
};
export default Users;

import React, { useState } from "react";
import ReactMapGL, { Marker } from "react-map-gl";
import "./Map.css";

const Map = (props) => {
  const [viewport, setViewport] = useState({
    latitude: props.coordinates.lat,
    longitude: props.coordinates.lng,
    zoom: props.zoom,
    width: props.width || "100%",
    height: props.height || "100%",
  });
  return (
    <div className={`map ${props.className}`} style={props.style}>
      <ReactMapGL
        {...viewport}
        mapboxApiAccessToken={
          "pk.eyJ1Ijoic2Fyb3JhMjk5NDQiLCJhIjoiY2twZWpheTkwMXM1ZTJvbGwzdzZkNHgzbiJ9.p7OjloPHkaD8N_q0XBNXpQ"
        }
        onViewportChange={(newView) => setViewport(newView)}
        mapStyle="mapbox://styles/sarora29944/ckp9wp1zq2lqt17o1lfmvx6j1"
      >
        <Marker
          latitude={props.coordinates.lat}
          longitude={props.coordinates.lng}
          offsetLeft={-20}
          offsetTop={-10}
        >
          <div className="marker">
            <span>
              <b></b>
            </span>
          </div>
        </Marker>
      </ReactMapGL>
    </div>
  );
};
export default Map;

import React, { useContext, useState } from "react";
import Card from "../../shared/components/UIElements/Card";
import Button from "../../shared/components/FormElements/Button";
import Modal from "../../shared/components/UIElements/Modal";
import Map from "../../shared/components/UIElements/Map";
import { AuthContext } from "../../shared/context/auth-context";
import "./PlaceItem.css";

const PlaceItem = (props) => {
  const auth = useContext(AuthContext);

  const [showModal, setShowModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const showModalHandler = () => {
    setShowModal(true);
  };
  const closeModalHandler = () => {
    setShowModal(false);
  };

  const showDeleteModalHandler = () => {
    setShowDeleteModal(true);
  };
  const cancelDeleteHandler = () => {
    setShowDeleteModal(false);
  };
  const confirmDeleteHandler = () => {
    setShowDeleteModal(false);
    console.log("Deleting...");
  };
  return (
    <React.Fragment>
      <li className={"place-item"}>
        <Card className={"place-item__content"}>
          <div className={"place-item__image"}>
            <img src={props.image} alt={props.title} />
          </div>
          <div className={"place-item__info"}>
            <h2>{props.title}</h2>
            <h3>{props.address}</h3>
            <p>{props.description}</p>
          </div>
          <div className={"place-item__actions"}>
            <Button onClick={showModalHandler} inverse>
              View on map
            </Button>
            {auth.isLoggedIn && (
              <React.Fragment>
                <Button to={`/places/${props.id}`}>Edit</Button>
                <Button danger onClick={showDeleteModalHandler}>
                  Delete
                </Button>
              </React.Fragment>
            )}
          </div>
        </Card>
      </li>

      {/* MAP MODAL */}
      <Modal
        onCancel={closeModalHandler}
        show={showModal}
        header={props.address}
        contentClass={"place-item__modal-content"}
        footerClass={"place-item__modal-actions"}
        footer={<Button onClick={closeModalHandler}>Close </Button>}
      >
        <div className={"map-container"}>
          <Map
            coordinates={props.coordinates}
            zoom={15}
            width={"100%"}
            height={"100%"}
            title={props.title}
          />
        </div>
      </Modal>

      {/* DELETE PLACE MODAL */}
      <Modal
        onCancel={cancelDeleteHandler}
        show={showDeleteModal}
        header={"Are you sure?"}
        contentClass={"place-item__modal-content"}
        footerClass={"place-item__modal-actions"}
        footer={
          <React.Fragment>
            <Button inverse onClick={cancelDeleteHandler}>
              Cancel
            </Button>
            <Button danger onClick={confirmDeleteHandler}>
              Delete
            </Button>
          </React.Fragment>
        }
      >
        <p>Are you sure you want to delete the place? It cant be undone</p>
      </Modal>
    </React.Fragment>
  );
};

export default PlaceItem;

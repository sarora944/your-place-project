import React from "react";
import { useParams } from "react-router-dom";
import PlaceList from "../components/PlaceList";

const DUMMY_PLACES = [
  {
    id: "p1",
    title: "Taj Mahal",
    description: "Its a Wonder!",
    imageUrl:
      "https://cdn.britannica.com/86/170586-050-AB7FEFAE/Taj-Mahal-Agra-India.jpg",
    address: "Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh 282001",
    location: {
      lat: 27.17324788074641,
      lng: 78.04244241456374,
    },
    creator: "u1",
  },
  {
    id: "p2",
    title: "Taj",
    description: "Its a Wonder!",
    imageUrl:
      "https://cdn.britannica.com/86/170586-050-AB7FEFAE/Taj-Mahal-Agra-India.jpg",
    address: "Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh 282001",
    location: {
      lat: 27.17324788074641,
      lng: 78.04244241456374,
    },
    creator: "u2",
  },
];
const Pages = (props) => {
  let userId = useParams().userId;
  let filteredUserPlaces = DUMMY_PLACES.filter(
    // (place) => place.creator === props?.match?.params?.userId
    (place) => place.creator === userId
  );
  return <PlaceList items={filteredUserPlaces} />;
};
export default Pages;

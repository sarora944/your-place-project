import React, { useEffect, useState } from "react";
import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import { useParams } from "react-router-dom";
import { useForm } from "../../shared/custom-hooks/form-hook";
import {
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE,
} from "../../shared/util/validators";
import "./PlaceForm.css";

const DUMMY_PLACES = [
  {
    id: "p1",
    title: "Taj Mahal",
    description: "Its a Wonder!",
    imageUrl:
      "https://cdn.britannica.com/86/170586-050-AB7FEFAE/Taj-Mahal-Agra-India.jpg",
    address: "Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh 282001",
    location: {
      lat: 27.17324788074641,
      lng: 78.04244241456374,
    },
    creator: "u1",
  },
  {
    id: "p2",
    title: "Taj",
    description: "Its a Wonder!",
    imageUrl:
      "https://cdn.britannica.com/86/170586-050-AB7FEFAE/Taj-Mahal-Agra-India.jpg",
    address: "Dharmapuri, Forest Colony, Tajganj, Agra, Uttar Pradesh 282001",
    location: {
      lat: 27.17324788074641,
      lng: 78.04244241456374,
    },
    creator: "u2",
  },
];

const UpdatePlace = () => {
  const [isLoading, setLoading] = useState(true);
  const placeId = useParams().placeId;
  const [formState, inputHandler, setFormData] = useForm(
    {
      title: {
        value: "",
        isValid: false,
      },
      description: {
        value: "",
        isValid: false,
      },
    },
    false
  );

  const filteredPlace = DUMMY_PLACES.find((el) => el.id === placeId);

  useEffect(() => {
    if (filteredPlace) {
      setFormData(
        {
          title: {
            value: filteredPlace.title,
            isValid: true,
          },
          description: {
            value: filteredPlace.description,
            isValid: true,
          },
        },
        true
      );
    }
    setLoading(false);
  }, [filteredPlace, setFormData]);

  const updateFormSubmitHandler = (e) => {
    e.preventDefault();
    console.log(formState.inputs);
  };

  if (!filteredPlace) {
    return <div className="place-form center">Could not find place</div>;
  }
  if (isLoading) {
    return <div className="center">Loading...</div>;
  }
  return (
    <form className="place-form" onSubmit={updateFormSubmitHandler}>
      <Input
        id="title"
        element="input"
        type="text"
        label="Title"
        onInput={inputHandler}
        validators={[VALIDATOR_REQUIRE()]}
        initialValue={formState.inputs.title.value}
        initialValid={formState.inputs.title.isValid}
        errorText="Please enter a valid title"
      />
      <Input
        id="description"
        element="textarea"
        label="Description"
        onInput={inputHandler}
        validators={[VALIDATOR_MINLENGTH(5)]}
        initialValue={formState.inputs.description.value}
        initialValid={formState.inputs.description.isValid}
        errorText="Please enter a description(atlease 5 characters)"
      />
      <Button type="submit" disabled={!formState.isValid}>
        Update Place
      </Button>
    </form>
  );
};
export default UpdatePlace;
